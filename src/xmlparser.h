#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <sys/types.h>
#include <apr_general.h>
#include <apr_pools.h>
#include <apr_hash.h>
#include <apr_strings.h>

#include "type.h"

#include <libxml/parser.h>
#include <libxml/tree.h>

#ifndef _XMLPARSER_H
#define _XMLPARSER_H

extern apr_pool_t *masterPool;
extern struct configuration *config;

u08 extractAction(xmlNode *xml_action, struct rule *this_rule, struct execute *this_execute);

u08 iterateOverXMLActions(xmlNode *xml_rule, struct rule *this_rule);

u08 extractRulePrefix(xmlNode *xml_rule, struct rule *this_rule);

u08 extractRuleMac(xmlNode *xml_rule, struct rule *this_rule);

u08 extractRule(xmlNode *xml_rule, struct rule *this_rule);

u08 extractMacList(xmlNode *xml_entry, struct maclist *this_maclist);

u08 iterateOverXMLEntries(xmlNode *root_element);

u08 parseXMLConfigFile(char *file_name);

#endif


