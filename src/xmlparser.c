#include "xmlparser.h"
#include "log.h"
#include "syslog.h"

/**
 * Helper function - is char c a hex character
 * @param c hex character value '0' -> 'f'
 * @return TRUE if this is a valid hex char
 */
static u08 isMacChar(char c)
{
	if(c >= '0' && c <= ':')
	{
		return TRUE;
	}
	else if(c >= 'A' && c <= 'F')
	{
		return TRUE;
	}
	else if(c >= 'a' && c <= 'f')
	{
		return TRUE;
	}
	else
	{
		return FALSE;
	}
}	

/**
 * Helper function - convert c to raw bits
 * @param c hex character value '0' -> 'f'
 * @return bit wise version 0x00 -> 0x0f
 */
static u08 getHexChar(char c)
{
	if(c >= '0' && c <= ':')
	{
		return c - '0';
	}
	else if(c >= 'A' && c <= 'F')
	{
		return (c - 'A') + 0x0A;
	}
	else if(c >= 'a' && c <= 'f')
	{
		return (c - 'a') + 0x0A;
	}
	else
	{
		return 0xFF;
	}
}	

/* Helper function to build action lists */
/**
 * Parse the individual action element - add data to the action list
 * @param xml_action libxml2 element corresponding to the action
 * @param this_rule storage for the newly created rule
 * @param this_execute storage for the command of an <execute/>
 * @returns TRUE if the element was parsable
 */
/* TODO: small memory leak .. storage for an <execute/> command isn't necessary for a <clear/> 
  Not critical, as this will only happen for 2 or 3 rules when the program is first started.
  More of a drip than a leak
*/
u08 extractAction(xmlNode *xml_action, struct rule *this_rule, struct execute *this_execute)
{
	if(!strcmp("clear", (char *)xml_action->name))
	{
		this_rule->clear = TRUE;
	}
	else if(!strcmp("execute", (char *)xml_action->name))
	{
		char *command = (char *)xmlNodeGetContent(xml_action); 
		this_execute->command = (char *)apr_pstrdup(config->pool,
						command);

		apr_tokenize_to_argv(this_execute->command,
					&(this_execute->args),
					config->pool);

		xmlFree(command);
	}
	else
	{
		fprintf(stderr, 
			"error: expected 'clear' or 'execute' (line %i)\n",
				xml_action->line);
			return FALSE;
	}

	return TRUE;
}

/** 
 * Extract the actions from a <rule/> and populate the action lists of the rule object
 * @param xml_rule libxml2 element corresponding to <rule/>
 * @param this_rule storage for the newly created rule
 * @returns TRUE if the action lists are valid
 */
u08 iterateOverXMLActions(xmlNode *xml_rule, struct rule *this_rule)
{
	u08 action_number = 0;
	struct execute *this_execute = NULL;
	xmlNode *xml_action = NULL;

	for(xml_action = xml_rule->children; 
			xml_action; 
			xml_action = xml_action->next)
	{
		if(xml_action->type == XML_ELEMENT_NODE)
		{
			this_execute = apr_pcalloc(config->pool, sizeof(struct execute));

			this_execute->index = action_number;
			action_number++;

			if(extractAction(xml_action, this_rule, this_execute) == FALSE)
			{
				fprintf(stderr, "error: Failed to load action %i for rule %i\n",
					action_number + 1, this_rule->config_index + 1);
				return FALSE;
			}
			else
			{
				if(this_execute->command == NULL)
				{
					/* it was a clear ... don't count it */
					action_number--;
				}
				else
				{
					apr_hash_set(this_rule->execute, 
						&(this_execute->index),
						sizeof(this_execute->index),
						this_execute);
				}
			}
		}
	}

	this_rule->number_actions = action_number;

	return TRUE;
}


/** 
 * Extract the prefix="" of a <rule/> and populate a rule object
 * @param xml_rule libxml2 element corresponding to <rule/>
 * @param this_rule storage for the newly created rule
 * @returns TRUE if the prefix="" was valid
 */
u08 extractRulePrefix(xmlNode *xml_rule, struct rule *this_rule)
{
	u08 i;
	char *prefix;
	char address[64], plen[4];

	prefix = (char *)xmlGetProp(xml_rule, (xmlChar *)"prefix");
	if(prefix != NULL)
	{
		/* read up to the first '/' */
		i = strstr(prefix, "/") - prefix;
		strncpy(address, prefix, i);
		address[i] = '\0';

		/* the remainder is the prefix length */
		strcpy(plen, (strstr(prefix, "/") + 1));

		xmlFree(prefix);

		i = inet_pton(AF_INET6, address, &(this_rule->prefix));
		this_rule->plen = atoi((char *)plen);
		if(i != 1)
		{
			LOG( RAMOND_CRITICAL "Failed "
				"to parse six addr: '%s'\n",
				address);
			return FALSE;
		}
	}
	else
	{
		return FALSE;
	}

	return TRUE;
}

/**
 * Build a hash table of `struct macaddress'es for the provided mac_string
 * If mac_string is a valid mac-list name, it sets the hash_t to point at 
 *      the already created mac-list
 * @param mac_string mac address or mac-list name
 * @param table where to create the new hash table
 * @param pool the pool the table should be created in
 * @returns TRUE if the table is valid
 */
u08 extractMacAddress(char *mac_string, apr_hash_t **table, apr_pool_t *pool)
{
	u08 error = FALSE;
	char *mac_string_p = mac_string;
	struct macaddress mac;
	u08 *mac_p = &(mac.cell[0]);

	/* mac addresses are of the form 00:11:22:33:44:55 */
	/* Check only hexchars and `:'s occur */
	while((*mac_string_p != '\0') && (error == FALSE))
	{
		if(isMacChar(*mac_string_p) == TRUE)
		{
			mac_string_p++;
		}
		else
		{
			error = TRUE;
		}
	}

	if(error == FALSE)
	{
		struct macaddress *specified_mac = NULL;

		/* Now re-read and pack a pair at a time into mac_p */
		mac_string_p = mac_string;

		while(*mac_string_p != '\0') 
		{
			char one = *mac_string_p;
			mac_string_p++;
			char two = *mac_string_p;
			mac_string_p++;
			char colon_or_null = *mac_string_p;
			if(colon_or_null == ':')
			{
				/* If the next character is a colon, we need to
					advance it by another char to get the 
					next data char */
				mac_string_p++;
			}

			u08 hex_one = getHexChar(one);
			u08 hex_two = getHexChar(two);

			*mac_p = hex_one<<4 | hex_two;
			mac_p++;
		}

		/* Copy the mac address into the provided pool and add to the table */
		specified_mac = apr_pmemdup(pool, &mac, sizeof(struct macaddress));

		*table = apr_hash_make(pool);

		apr_hash_set(*table, specified_mac, sizeof(struct macaddress), 
			specified_mac);
	}
	else
	{
		/* Not a mac address, is it a valid mac-list name ? */
		struct maclist *list = apr_hash_get(config->mac_lists, 
			mac_string, APR_HASH_KEY_STRING);

		if(list != NULL)
		{

			/* the name specified a maclist */
			*table = list->mac_addresses;
		}
		else
		{
			/* input wasn't a mac address or a valid mac-list name */

			/* Mac parsing error */
			LOG( RAMOND_CRITICAL "Failed to parse mac "
				"addr: '%s'\n",	mac_string);

			return FALSE;
		}
	}

	return TRUE;
}

/**
 * Populates the rule->mac_addresses hash table with details from
 * the mac="" property of a rule.
 * @param xml_rul libxml2 element corresponding to <rule/>
 * @param this_rule storage for the newly created rule
 * @returns TRUE if the rule->mac_addresses table is valid
 */
u08 extractRuleMac(xmlNode *xml_rule, struct rule *this_rule)
{
	char *mac_string = NULL;
	char *mac_string_p = NULL;
	struct macaddress this_mac;
	u08 *this_mac_p = &(this_mac.cell[0]);

	mac_string = (char *)xmlGetProp(xml_rule, (xmlChar *)"mac");
	if(mac_string != NULL)
	{	
		u08 r = extractMacAddress(mac_string, &(this_rule->mac_addresses), config->pool);
		xmlFree(mac_string);
		return r;
	}
	else
	{
		/* Not specifying a mac="" is quite valid, set the empty-set of mac
			addresses to disable the match check */
		this_rule->mac_addresses = apr_hash_make(config->pool);
		return TRUE;
	}
}


/** 
 * Examine a <rule/> and populate a `rule' object
 * @param xml_rule libxml2 element corresponding to <rule/>
 * @param this_rule storage for the newly created rule
 * @returns TRUE when the built rule is valid
 */
u08 extractRule(xmlNode *xml_rule, struct rule *this_rule)
{
	char *interface, *lifetime;

	/* Add prefix="" properties to the rule */
	if(extractRulePrefix(xml_rule, this_rule) == FALSE)
	{
		/* If no prefix is specified, set plen to -1, used to turn 
			the match-check off  */
		memset(&(this_rule->prefix), 0, sizeof(this_rule->prefix));
		this_rule->plen	= -1;
	}

	/* Add mac="" properties to the rule */
	if(extractRuleMac(xml_rule, this_rule) == FALSE)
	{
		/* Corrupt mac specified - abort the rule*/
		return FALSE;
	}

	/* Add interface="" properties to the rule */
	interface = (char *)xmlGetProp(xml_rule, (xmlChar *)"interface");
	if(interface != NULL)
	{
		this_rule->interface = if_nametoindex(interface);
		if(this_rule->interface == 0)
		{
			/* bad interface - abort the rule */
			LOG( RAMOND_CRITICAL "error: interface"
				" %s not recognized (line %i)\n",
				interface,
				xml_rule->line);
			xmlFree(interface);
			return FALSE;
		}
		xmlFree(interface);
	}
	else
	{
		/* interface id `-1' indicates the check should be disabled */
		this_rule->interface=-1;
	}

	/* Add lifetime="" properties to the rule */
	lifetime = (char *)xmlGetProp(xml_rule, (xmlChar *)"lifetime");
	if(lifetime != NULL)
	{
		this_rule->lifetime = atoi(lifetime);
		this_rule->check_lifetime = TRUE;
	
		xmlFree(lifetime);
	}
	else
	{
		/* lifetime could be any number, so we also have a check_lifetime flag 
			to turn the check off */
		this_rule->lifetime = 0;
		this_rule->check_lifetime = FALSE;
	}

	/* Allocate storage for this rules actions */
	this_rule->execute = apr_hash_make(config->pool);

	/* parse actions */
	if(iterateOverXMLActions(xml_rule, this_rule) == FALSE)
	{
		LOG( RAMOND_CRITICAL "error: failed to load"
			" actions for rule %i\n",
			this_rule->config_index+1);
		return FALSE;
	}

	return TRUE;
}

/** 
 * Loop over the contents of a <mac-list/> populating a `maclist' object
 * @param xml_entry libxml2 element corresponding to <mac-list/>
 * @param this_maclist storage for the newly created maclist
 * @returns TRUE when the built maclist is valid
 */
u08 extractMacList(xmlNode *xml_entry, struct maclist *this_maclist)
{
	u08 entry_number = 0;
	char *list_name = NULL;
	xmlNode *this_entry = NULL;
	
	/* Extract the name of this list */
	list_name = (char *)xmlGetProp(xml_entry, (xmlChar *)"name");
	if(list_name != NULL)
	{
		this_maclist->name = apr_pstrdup(config->pool, list_name);
		this_maclist->mac_addresses = apr_hash_make(config->pool);
		xmlFree(list_name);
	}
	else
	{
		LOG( RAMOND_CRITICAL "error: mac-lists must have a"
			" name property!\n");
		return FALSE;
	}

	/* Iterate over the contents of the <mac-list/> */
	for(this_entry = xml_entry->children; 
			this_entry; 
			this_entry = this_entry->next)
	{
		if(this_entry->type == XML_ELEMENT_NODE)
		{
			/* if the child element is an <entry/> ... */
			if(!strcmp("entry", this_entry->name))
			{
				u08 r = FALSE;
				apr_hash_t *tmp_table = NULL;
				apr_pool_t *tmp_pool = NULL;
				apr_pool_create(&tmp_pool, config->pool);

				entry_number++;

				/* Try to parse the <entry/> contents as a mac address */
				r = extractMacAddress(
					xmlNodeGetContent(this_entry),
					&tmp_table, tmp_pool);

				/* If successful, merge this result with the root
					hash_t 
					NOTE: don't call aprs _merge_ function, it would waste
					memory.
				*/
				if(r == TRUE)
				{
					apr_hash_index_t *hi;
					struct macaddress *mac;
					struct macaddress *mac_cp;

					/* loop over the temporary table, adding
						each child to the maclist table 
					   This automagically allows you to reference one
						mac list in another!
					*/
					for (hi = apr_hash_first(tmp_pool, tmp_table);
							hi;
							hi = apr_hash_next(hi))
					{
						apr_hash_this(hi, NULL, NULL, (void *)&mac);

						mac_cp = apr_pmemdup(config->pool, 
								mac, 
								sizeof(struct macaddress));

						apr_hash_set(this_maclist->mac_addresses,
							mac_cp,
							sizeof(struct macaddress),
							mac_cp);
					}
				}

				apr_pool_destroy(tmp_pool);
			}
		}
	}

	return TRUE;
}


/**
 * Loop over the contents of the xml file, and create objects to be added
 * into the config object.
 * Handles creation of rule and maclist objects.
 * @param root_element libxml2 list of xml elements of <rule/>s and <mac-list/>s
 * @returns TRUE always
 */
u08 iterateOverXMLEntries(xmlNode *root_element)
{
	/* The rules internal idea of which number it is */
	/* used for debugging ... `rule 8 failed to load */
	u08 rule_number = 0;

	/* The configs idea of which rule number this is */
	/* If rule 1 fails to load, we still want an entry 
		`1' in the rule list */
	u08 loaded_rule_number = 0;

	u08 failed_rules = 0;
	xmlNode *xml_entry = NULL;

	/* Loop over the contents of the config file */
	for(xml_entry = root_element->children; xml_entry; xml_entry = xml_entry->next)
	{
		if(xml_entry->type == XML_ELEMENT_NODE)
		{
			/* Is this a rule? */
			if(!strcmp("rule", (char *)xml_entry->name))
			{
				struct rule *this_rule = NULL;

				/* Allocate storage */
				this_rule = apr_pcalloc(config->pool, sizeof(struct rule));

				this_rule->config_index = rule_number;
				rule_number++;

				/* Ask the helper function to build a `rule' object */
				if(extractRule(xml_entry, this_rule) == FALSE)
				{
					failed_rules++;
					LOG( RAMOND_CRITICAL "error: Failed to load "
					       	"rule %i\n", rule_number);

					/* Continue, as we can probably load other rules */
				}
				else
				{
					/* Add the built `rule' object to the `config' objects
						rules hash table */
					this_rule->list_index = loaded_rule_number;
					loaded_rule_number++;

					apr_hash_set(config->rules, 
						&(this_rule->list_index),
						sizeof(this_rule->list_index),
						this_rule);
				}
			}
			/* Is it a mac-list ? */
			else if(!strcmp("mac-list", (char *)xml_entry->name))
			{
				struct maclist *this_maclist = NULL;

				/* Allocate storage */
				this_maclist = apr_pcalloc(config->pool, sizeof(struct maclist));

				/* Ask the helper function to build a `maclist' object */
				if(extractMacList(xml_entry, this_maclist) == FALSE)
				{
					LOG( RAMOND_CRITICAL "error: Failed to parse mac list\n");

					/* Continue, as we can probably load other rules */
				}
				else
				{
					/* Add the built `maclist' object to the `config' objects
						maclists hash table */

					apr_hash_set(config->mac_lists, 
						this_maclist->name,
						APR_HASH_KEY_STRING,
						this_maclist);
				}
			}
			else
			{
				/* Ignore it ... might be a config option for a later
					version of me! */
			}
		}
	}

	LOG( RAMOND_STARTUP "Loaded %i rules\n", rule_number - failed_rules);
	if(failed_rules != 0)
	{
		LOG( RAMOND_STARTUP "%i rules failed to loaded\n", failed_rules);
	}

	return TRUE;
}


/**
 * Build the `config' object from the contents of the xml config file.
 * Handles initialisation of libxml2 and the config object.
 * @param file_name path to the xml config file
 * @returns TRUE if the config object is valid
 */
u08 parseXMLConfigFile(char *file_name)
{
	xmlDoc *doc = NULL;
	apr_pool_t *config_pool;
	xmlNode *root_element = NULL;

	/*
	* this initialize the library and check potential ABI mismatches
	* between the version it was compiled for and the actual shared
	* library used.
	*/
	LIBXML_TEST_VERSION

	/*parse the file and get the DOM */
	doc = xmlReadFile(file_name, NULL, 0);

	if (doc == NULL)
	{
		LOG( RAMOND_CRITICAL "error: could not parse "
				" file %s\n", file_name);
		return FALSE;
	}
	else
	{
		char *log_file;

		apr_pool_create(&config_pool, masterPool);
		root_element = xmlDocGetRootElement(doc);

		LOG( RAMOND_STARTUP "Loading configuration "
					"from '%s'\n", file_name);

		config = apr_pcalloc(config_pool, sizeof(struct configuration));

		config->pool = config_pool;

		if(root_element->type == XML_ELEMENT_NODE)
		{
			if(!strcmp("ramond", (char *)root_element->name))
			{
				/* Extract the logfile path if present */
				log_file = (char *)xmlGetProp(root_element, (xmlChar *)"logfile");
				config->log_file = (char *)apr_pstrdup(config->pool, log_file);
				xmlFree(log_file);

				/* Create storage for the rules and mac_lists then
					loop over the contents of the config file */
				config->mac_lists = apr_hash_make(config->pool);
				config->rules = apr_hash_make(config->pool);

				return iterateOverXMLEntries(root_element);
			}
			else
			{
				LOG( RAMOND_CRITICAL "error: did not recognise "
					"root element on line %i\n", 
					root_element->line);
			}
		}

		LOG( RAMOND_CRITICAL "error: invalid root-node in XML") ;
		return FALSE;
	}
}




