#include <stdlib.h>
#include <stdio.h>
#include <stdarg.h>
#include <time.h>
#include <string.h>
#include <syslog.h>

#ifndef _LOG_H
#define _LOG_H


/*
 * Our four log levels:
 *
 * Startup & Critical are sent to syslog, logfile and stdout
 * Info & Action are only printed to stdout
 *   (you can redirect these somewhere if you want to keep them, 
 *    they are primarily for debugging)
 */
#define RAMOND_STARTUP 	"\001"
#define RAMOND_CRITICAL "\002"
#define RAMOND_INFO    	"\003"
#define RAMOND_ACTION  	"\004"


FILE *log_file;

#endif
