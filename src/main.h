#include <stdlib.h>
#include <stdio.h>
#include <errno.h>
#include <unistd.h>
#include <time.h>
#include <pcap.h>
#include <syslog.h>
#include <signal.h>

#include <sys/types.h>
#include <sys/socket.h>


#include <apr_general.h>
#include <apr_pools.h>

#include "type.h"
#include "xmlparser.h"

/* Microsoft use this in their RAs from XP boxes [rfc4191] */
#ifndef ND_OPT_ROUTE_INFO
#define ND_OPT_ROUTE_INFO 24
#define ND_RA_FLAG_RTPREF_MASK	0x18 /* 00011000 */

#define ND_RA_FLAG_RTPREF_HIGH	0x08 /* 00001000 */
#define ND_RA_FLAG_RTPREF_MEDIUM	0x00 /* 00000000 */
#define ND_RA_FLAG_RTPREF_LOW	0x18 /* 00011000 */
#define ND_RA_FLAG_RTPREF_RSV	0x10 /* 00010000 */
struct nd_opt_route_info     /* route information */
  {
	uint8_t	nd_opt_rti_type;
	uint8_t	nd_opt_rti_len;
	uint8_t	nd_opt_rti_prefixlen;
	uint8_t	nd_opt_rti_flags;
	uint32_t	nd_opt_rti_lifetime;
	/* followed by prefix */
  };
#endif

#ifndef _MAIN_H
#define _MAIN_H

FILE *log_file;

#endif


