#include <sys/types.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <apr_general.h>
#include <apr_hash.h>
#include <apr_pools.h>

#include <netinet/in.h>
#include <netinet/ip6.h>
#include <netinet/icmp6.h>

#include <net/if.h>
#include <net/ethernet.h>

#ifndef _TYPES_H
#define _TYPES_H

#ifndef ETHERTYPE_IPV6
#define ETHERTYPE_IPV6 ETH_P_IPV6
#endif

#define u08 unsigned char
#define s08 signed char
#define u16 unsigned short
#define s16 signed short
#define u32 unsigned long
#define s32 signed long
#define u64 unsigned long long
#define s64 signed long long

#ifndef IPV6_RECVPKTINFO
#define IPV6_RECVPKTINFO IPV6_PKTINFO
#endif

#ifndef FALSE
#define FALSE 0
#define TRUE !FALSE
#endif /* ifndef FALSE */

struct configuration
{
	/* rules is a hashtable of struct rules
		#(int rule_number->struct rule)
	*/
	apr_hash_t *rules;

	/* mac_lists is a hastable containing hashtables containing struct macaddresss 
		#(char *list-name->#(struct macaddress->struct macaddress))
	*/
	apr_hash_t *mac_lists;

	char *log_file;

	apr_pool_t *pool;
};

struct rule
{
	/* Our position in the config file */
	u08 config_index;

	/* Our position in the list of rules */
	u08 list_index;

	struct in6_addr prefix;
	/* we could use a s08 here, but it can't represent 128 */
	s16 plen;

	/* mac_addresses is a list of 0 or more struct mac_addresses that 
		should be matched
		#(struct macaddress->struct macaddress)
	*/
	apr_hash_t *mac_addresses;

	/* number_actions is just used as a boolean to whether
		there _are_ actions */
	u08 number_actions;
	apr_hash_t *execute;

	signed int interface;
	/* lifetime could be any number, so we also have a check_lifetime flag 
			to turn the check off */
	u16 lifetime;
	u08 check_lifetime;

	/* mac_addresses_exclude is a list of 0 or more struct mac_addresses 
		that should not be matched
		#(struct macaddress->struct macaddress)
	*/
	apr_hash_t *mac_addresses_exclude;
	u08 clear;
};

struct execute
{
	u08 index;

	char *command;
	char **args;
};

struct maclist
{
	char *name;
	/* mac_addresses is a list of 0 or more struct mac_addresses
		#(struct macaddress->struct macaddress)
	*/
	apr_hash_t *mac_addresses;
};

struct macaddress
{
	u08 cell[ETHER_ADDR_LEN];
};

struct ra_info
{
	/* This ra was recevied from: */
	struct in6_addr from;
	char from_str[NI_MAXHOST];

	/* On interface: */
	unsigned int interface;

	/* It advertised prefixes: */
	u08 number_prefixes;
	apr_hash_t *prefixes;

	/* It advertised routes: */
	u08 number_routes;
	apr_hash_t *routes;

	/* the first matched was: */
	struct ra_prefix *matched_prefix;

	/* With link layer address: */
	struct macaddress ll_addr;

	/* And mtu: */
	u32 mtu;

	/* The packet itself can be found at: */
	struct nd_router_advert *ra;
	int ra_len;

	apr_pool_t *pool;
};


struct ra_prefix
{
	u08 index;
	struct in6_addr prefix;
	char prefix_str[NI_MAXHOST];
	u08 flags;
	u08 plen;
};

struct ra_route
{
	u08 index;
	struct in6_addr prefix;
	char prefix_str[NI_MAXHOST];
	u08 plen;
	u08 preference;
};

struct ip6_pseudo_header
{
	struct in6_addr src;
	struct in6_addr dst;

	u32 plen;

	u16 pad1;
	u08 pad2;

	u08 next_header;

};

#endif /* _TYPES_H */

