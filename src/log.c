#include "log.h"


void LOG(const char *fmt, ...)
{
	va_list ap;
	time_t t;
	struct tm *tmp;
	char log_level = 0;
	char timestamp[32];
	char syslog_message[1024];

	memset(&syslog_message, 0, sizeof(syslog_message));

	t = time(NULL);
	tmp = localtime(&t);
	strftime((char *)&timestamp, sizeof(timestamp),
			"%Y-%m-%d-%H-%M-%S", tmp);

	if(*fmt <= 8)
	{
		log_level = (char)*fmt;
		fmt++;
	}

	va_start(ap, fmt);
	vsnprintf(syslog_message, sizeof(syslog_message), fmt, ap);
	va_end(ap);

	printf("%s: {%i} %s", timestamp, log_level, syslog_message);

	if(log_level == *RAMOND_STARTUP)
	{
		syslog(LOG_NOTICE, "%s", syslog_message);
		if(log_file != NULL)
		{
			fprintf(log_file, "%s: %s", timestamp, syslog_message);
		}
	}
	else if(log_level == *RAMOND_CRITICAL)
	{
		syslog(LOG_ERR, "%s", syslog_message);
		if(log_file != NULL)
		{
			fprintf(log_file, "%s: %s", timestamp, syslog_message);
		}
	}

	fflush(log_file);
}
