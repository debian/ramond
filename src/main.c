#include "main.h"
#include "log.h"

apr_pool_t *masterPool;
struct configuration *config;

static struct msghdr rcvmhdr;
static int rcvcmsglen;
static struct iovec rcviov[2];

static unsigned char packet[1500];

void listRules(void);

void usage(char *prog_name)
{
	fprintf(stderr, "%s [-h] [-c /etc/ramond.conf]\n", prog_name);
	fprintf(stderr, "	-h : print this help.\n");
	fprintf(stderr, "	-c : path to config file.\n");
}

void sigchld_handler(int sig)
{
	int d = 0;

	wait(&d);

	return;
}

u08 parseConfigFile(int argc, char *argv[])
{
	u08 i = 0;
	FILE *yyin= NULL;

	for(i = 0; i < argc; i++)
	{
		if(!strcmp("-c", argv[i]))
		{
			yyin = fopen(argv[i+1], "r");
			if(yyin != NULL)
			{
				parseXMLConfigFile(argv[i+1]);
				return TRUE;
			}
			else
			{
				fprintf(stderr, "No configuration file specified\n");
				return FALSE;
			}
		}

		if(!strcmp("-h", argv[i]))
		{
			return FALSE;
		}
	}

	if(yyin == NULL)
	{
		/* Look in default locations */
		yyin = fopen("ramond.conf", "r");
		if(yyin != NULL)
		{
			fclose(yyin);
			parseXMLConfigFile("ramond.conf");
			return TRUE;
		}
		else
		{
			yyin = fopen("/etc/ramond.conf", "r");
			if(yyin != NULL)
			{
				fclose(yyin);
				parseXMLConfigFile("/etc/ramond.conf");
				return TRUE;
			}
			else
			{
				fprintf(stderr, "Could not find config file\n");
				return FALSE;
			}
		}
	}
	return FALSE;
}

/**
 * Opens a raw socket to receive only ra messages with
 *
 * Marks the socket to store interface details.
 */
int rafixd_sockOpen()
{
	static unsigned char *rcvcmsgbuf = NULL;
	int s, on;
	struct icmp6_filter filt;

	/* calculate the space needed for ancillary data */
	rcvcmsglen = CMSG_SPACE(sizeof(struct in6_pktinfo)) + CMSG_SPACE(sizeof(int));
	if(rcvcmsgbuf == NULL && (rcvcmsgbuf = malloc(rcvcmsglen)) == NULL)
	{
		fprintf(stderr, "malloc for receive msghdr failed\n");
		exit(-1);
	}

	if((s = socket(AF_INET6, SOCK_RAW, IPPROTO_ICMPV6)) < 0)
	{
		fprintf(stderr, "socket: %s\n", strerror(errno));
		exit(-1);
	}

	/* we want to know the interface the packet came in from */
	on = 1;
	if (setsockopt(s, IPPROTO_IPV6, IPV6_RECVPKTINFO, &on, sizeof(on)) < 0)
	{
		fprintf(stderr, "IPV6_RECVPKTINFO: %s\n", strerror(errno));
		close(s);
		exit(-1);
	}

	/* specfiy to accept only router advertisements on the socket */
	ICMP6_FILTER_SETBLOCKALL(&filt);
	ICMP6_FILTER_SETPASS(ND_ROUTER_ADVERT, &filt);
	if (setsockopt(s, IPPROTO_ICMPV6, ICMP6_FILTER, &filt, sizeof(filt)) == -1)
	{
		fprintf(stderr,"setsockopt(ICMP6_FILTER): %s\n", strerror(errno));
		close(s);
		exit(-1);
	}

	/* initialize msghdr for receiving packets */
	rcviov[0].iov_base = (caddr_t)packet;
	rcviov[0].iov_len = sizeof(packet);
	rcvmhdr.msg_iov = rcviov;
	rcvmhdr.msg_iovlen = 1;
	rcvmhdr.msg_control = (caddr_t)rcvcmsgbuf;

	return s;
}

/**
 * Receives a router-advertisment on socket s
 * This code is heavily based on rafixd's
 *
 * Packs some data into fields in the ra_info struct
 */
struct ra_info *rafixd_recvRa(int s, struct ra_info *data)
{
	int len;
	struct sockaddr_in6 from;
	struct icmp6_hdr *icp;
	struct cmsghdr *cm;
	struct in6_pktinfo *packet_info;

	packet_info = NULL;

	rcvmhdr.msg_name = &from;
	rcvmhdr.msg_namelen = sizeof(from);
	rcvmhdr.msg_controllen = rcvcmsglen;

	if ((len = recvmsg(s, &rcvmhdr, 0)) < 0)
	{
 		LOG( RAMOND_CRITICAL "! socket recvmsg error: '%s'\n", 
				strerror(errno));
		return NULL;
	}

	/* extract packet information */
	for(cm = (struct cmsghdr *)CMSG_FIRSTHDR(&rcvmhdr);
	    cm && cm->cmsg_len;
	    cm = (struct cmsghdr *)CMSG_NXTHDR(&rcvmhdr, cm))
	{
		if (cm->cmsg_level == IPPROTO_IPV6 &&
		    cm->cmsg_type == IPV6_PKTINFO &&
		    cm->cmsg_len == CMSG_LEN(sizeof(struct in6_pktinfo)))
		{
			packet_info = (struct in6_pktinfo *)(CMSG_DATA(cm));
		}
	}

	if (packet_info == NULL)
	{
		LOG ( RAMOND_CRITICAL "! failed to get receiving"
				" packet info\n");
		return NULL;
	}

	icp = (struct icmp6_hdr *)rcvmhdr.msg_iov[0].iov_base;
	data->interface = packet_info->ipi6_ifindex;
	memcpy(&(data->from), &(from.sin6_addr), sizeof(struct in6_addr));
	getnameinfo((struct sockaddr *)&from, sizeof(from),
			data->from_str, NI_MAXHOST,
			NULL, 0, 
			NI_NUMERICHOST);
	

	/* Check the filtered raw socket is filtering packets */
	if (icp->icmp6_type != ND_ROUTER_ADVERT)
	{
		/* XXX: impossible */
		char interface_name[IF_NAMESIZE];

		LOG( RAMOND_CRITICAL "unexpected icmp type (%d) from"
			" %s on %s\n",
			icp->icmp6_type, data->from_str, 
			if_indextoname(data->interface, interface_name));

		return NULL;
	}
	else
	{
		/* Pack the ra_info struct */
		data->ra = (struct nd_router_advert *)icp;
		data->ra_len = len;
	}

	return data;

}

/**
 * Iterates over the option headers of the packet
 *
 * Packs the extra fields in the ra_info struct
 */
struct ra_info *parseHeaders(struct ra_info *data)
{
	int optlen;
	struct nd_opt_hdr *hdr = (struct nd_opt_hdr *)(data->ra + 1);
	int resid = data->ra_len - sizeof(struct nd_router_advert);
	int prefixlbyt;

	data->prefixes = apr_hash_make(data->pool);
	data->routes = apr_hash_make(data->pool);

	/* Iterate over the available option headers */
	for (optlen = 0; resid > 0; resid -= optlen)
	{

		/* Don't run off the end of the buffer */
		if (resid < sizeof(struct nd_opt_hdr))
		{
			break;
		}

		hdr = (struct nd_opt_hdr *)((caddr_t)hdr + optlen);
		optlen = hdr->nd_opt_len << 3;

		/* Bad option */
		if (hdr->nd_opt_len == 0)
		{
			break;
		}

		/* Bad option, bits of it must be missing */
		if (resid < optlen)
		{
			break;
		}

		/* RFC2461 allows:
			* Source link layer address
			* MTU
			* Prefix Information
		   RFC41919 adds:
		   	* Router Preference Option
		*/
		switch(hdr->nd_opt_type)
		{
			case ND_OPT_SOURCE_LINKADDR:
			{
				/* Copy the data */
				memcpy(&(data->ll_addr), hdr+1, sizeof(struct macaddress));

				break;
			}
			case ND_OPT_PREFIX_INFORMATION:
			{
				struct nd_opt_prefix_info *p = 
						(struct nd_opt_prefix_info *)hdr;
				struct ra_prefix *prf = apr_pcalloc(data->pool,
								sizeof(struct ra_prefix));

				/* Copy the data */
				memcpy(&(prf->prefix), &(p->nd_opt_pi_prefix), 
					sizeof(struct in6_addr));
				inet_ntop(AF_INET6, &(prf->prefix), prf->prefix_str, sizeof(prf->prefix_str));
				prf->plen = p->nd_opt_pi_prefix_len;
				prf->flags = p->nd_opt_pi_flags_reserved;

				prf->index = data->number_prefixes;
				data->number_prefixes++;

				/* Add the prefix */
				apr_hash_set(data->prefixes,
					&(prf->index),
					sizeof(prf->index),
					prf);
				break;
			}
			case ND_OPT_MTU:
			{
				struct nd_opt_mtu *p = 
						(struct nd_opt_mtu *)hdr;

				/* Copy the data */
				data->mtu = p->nd_opt_mtu_mtu;

				break;

			}
			case ND_OPT_ROUTE_INFO:
			{
				struct nd_opt_route_info *p = 
						(struct nd_opt_route_info *)hdr;
				struct ra_route *rrt = apr_pcalloc(data->pool,
								sizeof(struct ra_route));

				/* Copy the data */
				memset(&(rrt->prefix), 0, sizeof(struct in6_addr));
				prefixlbyt = sizeof(struct in6_addr);
				if (p->nd_opt_rti_prefixlen < (prefixlbyt*8))
					prefixlbyt = (p->nd_opt_rti_prefixlen>>3);
				if (((p->nd_opt_rti_prefixlen) % 8) != 0) prefixlbyt++;
				if ((prefixlbyt % 8) != 0)
				prefixlbyt = ((prefixlbyt>>3)+1)*8;
				if (prefixlbyt > sizeof(struct in6_addr)) prefixlbyt = sizeof(struct in6_addr);
				memcpy(&(rrt->prefix), 
					((u08 *)p + sizeof(struct nd_opt_route_info)), 
					prefixlbyt);
				inet_ntop(AF_INET6, &(rrt->prefix), rrt->prefix_str, sizeof(rrt->prefix_str));
				rrt->plen = p->nd_opt_rti_prefixlen;
				rrt->preference = ((p->nd_opt_rti_flags & ND_RA_FLAG_RTPREF_MASK) >> 3);

				rrt->index = data->number_routes;
				data->number_routes++;

				/* Add the route */
				apr_hash_set(data->routes,
					&(rrt->index),
					sizeof(rrt->index),
					rrt);
				break;
			}
			default:
			{
				/* 'silently' discard unknown options */
				LOG( RAMOND_INFO "Unrecognised option type '%i'\n", hdr->nd_opt_type);
				break;
			}
		}
	}

	return data;
}

/**
 * Examines two v6 address to determine if they exist on the same
 * subnet.
 *
 * Masks both addresses (on the stack) with plen, then compares them
 *
 * returns TRUE or FALSE
 */
u08 areAddressesAdjacent(struct in6_addr one, struct in6_addr two, u08 plen)
{
	u08 i = 0;
	s16 c = plen;

	/* drop the non prefix bits, then compare */
	for(; c >= 8; c -= 8)
	{
		one.s6_addr[i++] &= 0xff;
	}

	if(c > 0)
	{
		one.s6_addr[i++] &= ~((1<<(8 - c)) -1);
	}

	while(i < 16)
	{
		one.s6_addr[i++] &= 0x00;
	}


	i = 0;
	c = plen;
	for(; c >= 8; c -= 8)
	{
		two.s6_addr[i++] &= 0xff;
	}

	if(c > 0)
	{
		two.s6_addr[i++] &= ~((1<<(8 - c)) -1);
	}

	while(i < 16)
	{
		two.s6_addr[i++] &= 0x00;
	}

	if(memcmp(&one, &two, sizeof(struct in6_addr)) == 0)
	{
		return TRUE;
	}
	else
	{
		return FALSE;
	}
}

/**
 * Walks all the rules, and returns a pointer to the first
 * matching rule, or NULL
 */
struct rule *matchRules(struct ra_info *data)
{
	u08 rule_number = 0;
	struct rule *matching_rule = NULL;
	struct rule *this_rule = NULL;
 
	this_rule = (struct rule *)apr_hash_get(config->rules,
					&rule_number,
					sizeof(rule_number));

	while(this_rule != NULL)
	{
		u08 i = 0;
		u08 prefix_match = FALSE;
		u08 mac_match = FALSE;
		u08 interface_match = FALSE;
		u08 lifetime_match = FALSE;
 
		/* Check the prefixes */
		if(this_rule->plen != -1)
		{
			for(i = 0 ; i < data->number_prefixes; i++)
			{
				struct ra_prefix *prf = 
					apr_hash_get(data->prefixes,
							&i, sizeof(i));
	
				if(areAddressesAdjacent(prf->prefix, 
							this_rule->prefix, 
							this_rule->plen))
				{
					prefix_match = TRUE;
					data->matched_prefix = prf;
					break;
				}
			}
		}
		else
		{
			/* We weren't interested in the prefix */
			prefix_match = TRUE;

			/* Because the prefix gets passed to the user script 
				we need to set it to _something_ */
			data->matched_prefix = NULL;
		}

		/* Check the source mac */
		if(apr_hash_count(this_rule->mac_addresses) != 0)
		{
			struct macaddress *src = apr_hash_get(
				this_rule->mac_addresses, 
				&(data->ll_addr), 
				sizeof(struct macaddress));

			if(src == NULL)
			{
				mac_match = FALSE;
			}
			else
			{
				mac_match = TRUE;
			}
		}
		else
		{
			/* We weren't interested in the source mac */
			mac_match = TRUE;
		}

		/* Check the interface */
		if(this_rule->interface != -1)
		{
			if(this_rule->interface == data->interface)
			{
				interface_match = TRUE;
			}
			else
			{
				interface_match = FALSE;
			}
		}
		else
		{
			/* We weren't interested in the interface */
			interface_match = TRUE;
		}
 
		/* Check the lifetime */
		if(this_rule->check_lifetime != FALSE)
		{
			if(this_rule->lifetime == data->ra->nd_ra_router_lifetime)
			{
				lifetime_match = TRUE;
			}
			else
			{
				lifetime_match = FALSE;
			}
		}
		else
		{
			/* We weren't interested in the lifetime */
			lifetime_match = TRUE;
		}

		if(prefix_match && mac_match && interface_match && lifetime_match)
		{
			matching_rule = this_rule;
			break;
		}
		else
		{
			/* Advance to the next rule */
			rule_number++;

			this_rule = (struct rule *)apr_hash_get(config->rules,
					&rule_number,
					sizeof(rule_number));
		}
	}

	return matching_rule;
}

/**
 * Forks and Execs the provided 'execute' command with the 
 * appropriate environmental variables set
 */
void executeActions(struct rule *this_rule, struct ra_info *data)
{
	u08 action_number = 0;
	char plen_str[8];
	char mac_address[32];
	char interface_name[IF_NAMESIZE];
	struct execute *this_action = NULL;
	struct ra_prefix;

	sprintf(mac_address, "%02x:%02x:%02x:%02x:%02x:%02x",
		data->ll_addr.cell[0], data->ll_addr.cell[1],
		data->ll_addr.cell[2], data->ll_addr.cell[3],
		data->ll_addr.cell[4], data->ll_addr.cell[5]);

	if(data->matched_prefix != NULL)
	{
		sprintf(plen_str, "%i", data->matched_prefix->plen);
		setenv("PREFIX", data->matched_prefix->prefix_str, 1);
		setenv("PREFIX_LEN", plen_str, 1);
	}
	else
	{
		setenv("PREFIX", NULL, 1);
		setenv("PREFIX_LEN", NULL, 1);
	}

	setenv("SOURCE_ADDR", data->from_str, 1);
	setenv("SOURCE_MAC", mac_address, 1);
	setenv("INTERFACE", if_indextoname(data->interface, interface_name), 1);

	this_action = (struct execute *)apr_hash_get(this_rule->execute,
					&action_number,
					sizeof(action_number));

	while(this_action != NULL)
	{
		pid_t p;

		LOG( RAMOND_ACTION "executing: '%s'\n", this_action->command);

		p = fork();
		if(p == 0)
		{
			if(execv(this_action->args[0], this_action->args) == -1)
			{
				LOG( RAMOND_CRITICAL "failed to execute '%s': '%s'\n", this_action->args[0], strerror(errno));
			}
		}
		else if(p == -1)
		{
			LOG( RAMOND_CRITICAL "failed to fork: '%s'\n", strerror(errno));
		}

		action_number++;

		this_action = (struct execute *)apr_hash_get(this_rule->execute,
						&action_number,
						sizeof(action_number));
	}


	return;
}

void rafixd_clearRoute(struct ra_info *data)
{
	int i;
	u08 hashi;
	pcap_t *fd;
	u08 *sendbuf = NULL;
	u32 lhlen, sendlen, slen, ralen;
	char ifname[IF_NAMESIZE];
	register u32 cksum = 0;
	struct ether_header *ether;
	struct ip6_hdr *ip6;
	struct nd_router_advert *ra;
	struct ip6_pseudo_header phdr;
	char errbuf[PCAP_ERRBUF_SIZE];
	u08 *options=NULL, *new_options, *optins;
	int options_len=0, options_len_inc;

	memset(&errbuf, 0, sizeof(errbuf));
	if_indextoname(data->interface, (char *)&ifname);

	/* open a binary packet injection socket */
	fd = pcap_open_live((char *)&ifname, 65535, 1, 0, (char *)&errbuf);
	if(fd == NULL)
	{
		LOG( RAMOND_CRITICAL "aborted clear: no injection socket\n");
		return;
	}

        /* construct zero-lifetime prefix and route info options */
	for(hashi = 0 ; hashi < data->number_prefixes; hashi++)
	{
		struct ra_prefix *prf = 
			apr_hash_get(data->prefixes, &hashi, sizeof(hashi));
		struct nd_opt_prefix_info *hdr;
		options_len_inc = sizeof(struct nd_opt_prefix_info);
		new_options = realloc(options, options_len + options_len_inc);
		if (new_options == NULL)
		{
			LOG( RAMOND_CRITICAL "failed to allocate"
				" buffer sized '%lu'bytes\n", options_len + options_len_inc);
			if (options != NULL) free(options);
			pcap_close(fd);
			return;
		}
		options = new_options;
 		hdr = (struct nd_opt_prefix_info *)(options + options_len); 
		options_len += options_len_inc;
		hdr->nd_opt_pi_type = ND_OPT_PREFIX_INFORMATION;
		hdr->nd_opt_pi_len = options_len_inc/8;
		hdr->nd_opt_pi_prefix_len = prf->plen;
		hdr->nd_opt_pi_flags_reserved = prf->flags;

		/* 2 hours in seconds, read RFC4862 section e 2 */
		hdr->nd_opt_pi_valid_time = 7200;
		hdr->nd_opt_pi_preferred_time = 0;
		hdr->nd_opt_pi_reserved2 = 0;
		memcpy(&(hdr->nd_opt_pi_prefix), &(prf->prefix),
					sizeof(struct in6_addr));
	}
	for(hashi = 0 ; hashi < data->number_routes; hashi++)
	{
		struct ra_route *rrt = 
			apr_hash_get(data->routes, &hashi, sizeof(hashi));
		struct nd_opt_route_info *hdr = (struct nd_opt_route_info *)(options + options_len); 
		int prefixlen = rrt->plen;
		int prefixlbyt = (prefixlen>>3);
		if ((prefixlen % 8) != 0) prefixlbyt++;
		if ((prefixlbyt % 8) != 0)
			prefixlbyt = ((prefixlbyt>>3)+1)*8;
		if (prefixlbyt > sizeof(struct in6_addr)) prefixlbyt = sizeof(struct in6_addr);

		options_len_inc = (sizeof(struct nd_opt_route_info) + prefixlbyt);
		new_options = realloc(options, options_len + options_len_inc);
		if (new_options == NULL)
		{
			LOG( RAMOND_CRITICAL "failed to allocate"
				" buffer sized '%lu'bytes\n", options_len + options_len_inc);
			if (options != NULL) free(options);
			pcap_close(fd);
			return;
		}
		options = new_options;
 		hdr = (struct nd_opt_route_info *)(options + options_len); 
		options_len += options_len_inc;
		hdr->nd_opt_rti_type = ND_OPT_ROUTE_INFO;
		hdr->nd_opt_rti_len = options_len_inc/8;
		hdr->nd_opt_rti_prefixlen = rrt->plen;
		/* Preference as in the incoming RA */
		hdr->nd_opt_rti_flags = (rrt->preference << 3);
		hdr->nd_opt_rti_lifetime = 0;
		memcpy(((u08 *)hdr + sizeof(struct nd_opt_route_info)), 
                       &(rrt->prefix),
			prefixlbyt);
	}

	lhlen = sizeof(struct ether_header);
	ralen = sizeof(struct nd_router_advert) + options_len;
	sendlen = lhlen + sizeof(struct ip6_hdr) + ralen;

	sendbuf = calloc(sendlen, 1);
	if(sendbuf == NULL)
	{
		LOG( RAMOND_CRITICAL "failed to allocate"
				" buffer sized '%lu'bytes\n", sendlen);
		if (options != NULL) free(options);
		pcap_close(fd);
		return;
	}

	/* construct the IPv6 header */
	ip6 = (struct ip6_hdr *)(sendbuf + lhlen);
	ip6->ip6_flow = htonl(0x60000000); /* ip6->ip6_vfc = IPV6_VERSION << 4; */

	ip6->ip6_nxt = IPPROTO_ICMPV6;
	ip6->ip6_hlim = 255;
	memcpy(&(ip6->ip6_src), &(data->from), sizeof(struct in6_addr));
	i = inet_pton(AF_INET6, "ff02::1", &(ip6->ip6_dst));
	ip6->ip6_plen = htons(ralen);

	/* construct the RA header */
	ra = (struct nd_router_advert *)(ip6 + 1);
	ra->nd_ra_type = ND_ROUTER_ADVERT;
	ra->nd_ra_curhoplimit = 64;
	ra->nd_ra_router_lifetime = 0;

	/* construct the ETHERNET header */
	ether = (struct ether_header *)sendbuf;
	ether->ether_type = htons(ETHERTYPE_IPV6);
	/* generate the link layer destination from the bottom
		32 bits of the v6 destination, as per multicast
		rules */
	if(IN6_IS_ADDR_MULTICAST(&(ip6->ip6_dst)))
	{
		ether->ether_dhost[0] = 0x33;
		ether->ether_dhost[1] = 0x33;
		ether->ether_dhost[2] = ip6->ip6_dst.s6_addr[12];
		ether->ether_dhost[3] = ip6->ip6_dst.s6_addr[13];
		ether->ether_dhost[4] = ip6->ip6_dst.s6_addr[14];
		ether->ether_dhost[5] = ip6->ip6_dst.s6_addr[15];
	}
	else
	{
		ether->ether_dhost[0] = ip6->ip6_dst.s6_addr[8] & 0xfd;
		ether->ether_dhost[1] = ip6->ip6_dst.s6_addr[9];
		ether->ether_dhost[2] = ip6->ip6_dst.s6_addr[10];
		ether->ether_dhost[3] = ip6->ip6_dst.s6_addr[13];
		ether->ether_dhost[4] = ip6->ip6_dst.s6_addr[14];
		ether->ether_dhost[5] = ip6->ip6_dst.s6_addr[15];
	}		

	/* set the source address to that of the rogue router */
	for(i = 0; i < ETHER_ADDR_LEN; i++)
	{
		ether->ether_shost[i] = data->ll_addr.cell[i];
	}


	/* calculate the packet ll checksum */
	memset(&phdr, 0, sizeof(struct ip6_pseudo_header));

	/* this is the ipv6 psuedo header, included in all checksums */
	memcpy(&(phdr.src), &(ip6->ip6_src), sizeof(struct in6_addr));
	memcpy(&(phdr.dst), &(ip6->ip6_dst), sizeof(struct in6_addr));
	phdr.plen = ip6->ip6_plen;
	phdr.next_header = IPPROTO_ICMPV6;

	/* this implementation is based on that in rfc1071 */
	i = 0;
	cksum = 0;
	while((2*i) < sizeof(struct ip6_pseudo_header))
	{
		cksum += (u16) ((u16 *)&phdr)[i];
		i++;
	}

	/* Insert options field */
	if (options_len > 0)
	{
		optins = (u08 *)(ra + 1);
		memcpy(optins, options, options_len);
	}
	if (options != NULL) free(options);

	i = 0;
	while((2*i) < ralen)
	{
		cksum += (u16) ((u16 *)ra)[i];
		i++;
	}

	/*  Add left-over byte, if any */
	if((2*i - ralen) > 0)
	{
		cksum += (u08) ((u08 *)ra)[(2*i)+1];
	}

	/*  Fold 32-bit sum to 16 bits */
	while(cksum>>16)
	{
		cksum = (cksum & 0xffff) + (cksum >> 16);
	}

	ra->nd_ra_hdr.icmp6_cksum = ~cksum;

	slen = pcap_inject(fd, sendbuf, sendlen);
	if(slen < 0)
	{
		LOG( RAMOND_CRITICAL "packet injection failed: %s\n", 
				strerror(errno));
	}

	free(sendbuf);
	pcap_close(fd);
}

int main(int argc, char *argv[])
{
	int socket;
	struct ra_info data;

	if(argc > 6)
	{
		usage(argv[0]);
		fprintf(stderr, "Too many command line arguments.\nExiting.\n");
		exit(1);
	}

	apr_initialize();
	atexit(apr_terminate);
	apr_pool_create(&masterPool, NULL);

	signal(SIGCHLD, sigchld_handler);

	/* Find the config file */
	if(!parseConfigFile(argc,argv))
	{
		usage(argv[0]);
		LOG( RAMOND_CRITICAL "Syntax error in configuration file\n");
		exit(1);
	}

	if(config->log_file == NULL)
	{
		/* No log file specified */
	}
	else
	{
		log_file = fopen(config->log_file, "wa");
		if(log_file == NULL)
		{
			LOG( RAMOND_CRITICAL "Failed to open logfile '%s'\n", 
					config->log_file);
			exit(1);
		}
	}

	LOG( RAMOND_STARTUP "-- starting --\n");
	LOG( RAMOND_STARTUP "-- opening socket --\n");
	socket = rafixd_sockOpen();
	if(socket < 0)
	{
		fprintf(stderr, "Failed to open socket\n");
		exit(1);
	}

	while(1)
	{
		memset(&data, 0, sizeof(data));
		apr_pool_create(&(data.pool), masterPool);

		LOG( RAMOND_INFO "-- listening --\n");
		if(rafixd_recvRa(socket, &data) != NULL)
		{
			struct rule *matching_rule;
		
			parseHeaders(&data);
			LOG( RAMOND_INFO "received a packet from %s\n", 
					data.from_str);
	
 			matching_rule = matchRules(&data);
			if(matching_rule != NULL)
			{

				LOG( RAMOND_INFO "Matched rule %i\n", 
						matching_rule->config_index+1);
				if(matching_rule->number_actions > 0)
				{
					LOG( RAMOND_ACTION "Executing actions\n");
					executeActions(matching_rule, &data);
				}
				if(matching_rule->clear)
				{
					LOG( RAMOND_ACTION "Clearing route\n");
					rafixd_clearRoute(&data);
				}
			}
			else
			{
				LOG( RAMOND_INFO "This packet does not "
						" match any rules.\n");
			}
		}

		apr_pool_destroy(data.pool);
	}

 	fclose(log_file);

	return 0;
}
