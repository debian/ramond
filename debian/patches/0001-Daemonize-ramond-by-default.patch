From: Nicolas Dandrimont <nicolas.dandrimont@crans.org>
Date: Fri, 5 Aug 2011 09:45:13 +0200
Subject: Daemonize ramond by default

This patch also adds a -d option to disable daemonizing.

Update on 2024-05-01: Replace broken flock(2) usage with lockf(3).

Last-Update: 2024-05-01
---
 src/main.c | 81 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++--
 src/main.h |  1 +
 2 files changed, 80 insertions(+), 2 deletions(-)

diff --git a/src/main.c b/src/main.c
index 0cb6e8a..2703d0e 100644
--- a/src/main.c
+++ b/src/main.c
@@ -1,6 +1,5 @@
 #include "main.h"
 #include "log.h"
-
 apr_pool_t *masterPool;
 struct configuration *config;
 
@@ -14,8 +13,9 @@ void listRules(void);
 
 void usage(char *prog_name)
 {
-	fprintf(stderr, "%s [-h] [-c /etc/ramond.conf]\n", prog_name);
+	fprintf(stderr, "%s [-h] [-d] [-c /etc/ramond.conf]\n", prog_name);
 	fprintf(stderr, "	-h : print this help.\n");
+	fprintf(stderr, "	-d : do not daemonize.\n");
 	fprintf(stderr, "	-c : path to config file.\n");
 }
 
@@ -824,11 +824,74 @@ void rafixd_clearRoute(struct ra_info *data)
 	pcap_close(fd);
 }
 
+/**
+ * daemonize ramond.
+ */
+void daemonize(void)
+{
+	pid_t pid, sid;
+	int i, pidfile, ret;
+
+	char pidstr[32];
+
+	pid = fork();
+
+	if(pid < 0)
+		exit(EXIT_FAILURE);
+	else if(pid > 0)
+		exit(EXIT_SUCCESS);
+
+	umask(027);
+	if((chdir("/")) < 0)
+		exit(EXIT_FAILURE);
+
+	sid = setsid();
+	if(sid < 0)
+		exit(EXIT_FAILURE);
+
+	pid = fork();
+
+	if(pid < 0)
+		exit(EXIT_FAILURE);
+	else if(pid > 0)
+		exit(EXIT_SUCCESS);
+
+	/* Cleanup open FDs */
+	for(i = getdtablesize(); i>=0; --i)
+		close(i);
+
+	i = open("/dev/null", O_RDWR); /* (re)open stdin */
+	ret = dup(i); /* stdout */
+	if(ret < 0)
+		exit(EXIT_FAILURE);
+	ret = dup(i); /* stderr */
+	if(ret < 0)
+		exit(EXIT_FAILURE);
+
+	pidfile = open("/var/run/ramond.pid", O_RDWR|O_CREAT, 0640);
+	if(pidfile < 0)
+		exit(EXIT_FAILURE);
+	if(lockf(pidfile, F_TLOCK, 0) < 0)
+		exit(EXIT_SUCCESS);
+
+	sprintf(pidstr, "%d\n", getpid());
+	ret = write(pidfile, pidstr, strlen(pidstr));
+	if(ret < strlen(pidstr))
+		exit(EXIT_FAILURE);
+
+	signal(SIGTSTP,SIG_IGN); /* ignore tty signals */
+	signal(SIGTTOU,SIG_IGN);
+	signal(SIGTTIN,SIG_IGN);
+}
+
 int main(int argc, char *argv[])
 {
 	int socket;
 	struct ra_info data;
 
+	int debug = 0;
+	int i = 0;
+
 	if(argc > 6)
 	{
 		usage(argv[0]);
@@ -842,6 +905,20 @@ int main(int argc, char *argv[])
 
 	signal(SIGCHLD, sigchld_handler);
 
+	for(i = 0; i < argc; i++)
+	{
+		if(!strcmp(argv[i], "-d"))
+		{
+			debug = 1;
+			break;
+		}
+	}
+
+	if(!debug)
+	{
+		daemonize();
+	}
+
 	/* Find the config file */
 	if(!parseConfigFile(argc,argv))
 	{
diff --git a/src/main.h b/src/main.h
index 26de811..6552d5b 100644
--- a/src/main.h
+++ b/src/main.h
@@ -1,5 +1,6 @@
 #include <stdlib.h>
 #include <stdio.h>
+#include <fcntl.h>
 #include <errno.h>
 #include <unistd.h>
 #include <time.h>
