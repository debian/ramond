GCC=/usr/bin/gcc
RM=/bin/rm

CFLAGS=-ggdb
LDFLAGS=-lpcap

APR_CFLAGS=`apr-1-config --includes --cppflags --cflags`
APR_LDFLAGS=`apr-1-config --link-ld --libs `

XML_CFLAGS=`xml2-config --cflags`
XML_LDFLAGS=`xml2-config --libs`

all: ramond

log.o: src/log.c
	$(GCC) -c src/log.c $(CFLAGS) $(APR_CFLAGS) $(XML_CFLAGS)

xmlparser.o: src/xmlparser.c src/type.h
	$(GCC) -c src/xmlparser.c $(CFLAGS) $(APR_CFLAGS) $(XML_CFLAGS)

main.o: src/main.c src/type.h
	$(GCC) -c src/main.c $(CFLAGS) $(APR_CFLAGS) $(XML_CFLAGS)


ramond: main.o xmlparser.o log.o
	$(GCC) log.o xmlparser.o main.o -o ramond $(LDFLAGS) $(APR_LDFLAGS) $(XML_LDFLAGS)

clean:
	@-$(RM) ramond
	@-$(RM) *.o
